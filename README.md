# SMARTEDGE-Public

## DataOps Toolbox

The folder `dataops` contains public artefacts associated with the implementation of the DataOps toolbox. The artefacts are referenced as submodules since they are hosted in dedicated repositories on GitHub.
- [`dataops/mapping-template`](./dataops) Library mapping-template for data and schema transformations used as a component in artefact A3.5
- [`dataops/chimera`](./dataops) Chimera framework for building DataOps pipelines, provides components for artefact A3.5. It contains also relevant resources for the low-code composition of the pipelines (artefact A3.7)
- [`dataops/deployment-templates`](./dataops) Deployment templates for DataOps pipelines (artefact A3.6).
